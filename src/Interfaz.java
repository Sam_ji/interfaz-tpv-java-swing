import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.SpringLayout.Constraints;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;


public class Interfaz extends JFrame {

	
	private JPanel panelArriba;
	private JPanel panelAbajo;
	private JTextField importe;
	private JPanel panelLateral;
	
	private JButton borrar;
	private JButton login;
	private JButton quiensoy;
	private JButton cobrar;
	private JButton descuento;
	private JTextField cDescuento;
	private JTextField nombre;
	

	
	
	private ArrayList<Producto> productosDisponibles;
	private ArrayList<BotonProducto> botonAddPedido;
	private ArrayList<BotonProducto> botonDeletePedido;
	
	private static Interfaz ref;

	
	
	
	
	public static Interfaz GetSingleton()
		{
		if(ref == null)
			ref = new Interfaz();
		return ref;
		}
	
	
	private Interfaz()
		{
			super("TPV");
			this.setVisible(true);
			this.setSize(900, 600);
			this.setLayout(new GridBagLayout());
			this.setDefaultCloseOperation(EXIT_ON_CLOSE);
			

			panelArriba = new JPanel();
			panelAbajo = new JPanel();
			panelLateral = new JPanel();
			importe = new JTextField();
			botonAddPedido = new ArrayList<BotonProducto>();
			botonDeletePedido = new ArrayList<BotonProducto>();
			productosDisponibles = new ArrayList<Producto>();
			
			JLabel prod = new JLabel("PRODUCTOS");
			JLabel ped = new JLabel("PEDIDO");
			
			borrar = new JButton("Borrar el pedido");
			login = new JButton("Cambiar de usuario");
			String s = new String("Introduzca su nombre");
			Font f = new Font("Verdana", Font.ITALIC, 12);
			
			nombre = new JTextField();
			nombre.setFont(f);
			nombre.setText(s);
			quiensoy = new JButton("PEPE");
			cobrar = new JButton("COBRAR EL PEDIDO");

			descuento = new JButton("Aplicar descuento");
			cDescuento = new JTextField();
			cDescuento.setFont(f);
			cDescuento.setText("Introduzca el c�digo de descuento");
			panelLateralListeners();
			
			
			panelAbajo.setLayout(new GridLayout(5,5,10,10));
			panelAbajo.setVisible(true);

			
			
			panelArriba.setLayout(new GridLayout(5,5,10,10));
			

			importe.setSize(40, 40);
			
			CreaProductos();
			InicializarBotonesArriba();
			importe.setText(Float.toString(0));
			BotonesPanelArriba();
			
			panelLateral.setLayout(new GridLayout(7, 1,5,5));
			panelLateral.add(quiensoy);
			panelLateral.add(nombre);
			panelLateral.add(login);
			panelLateral.add(cDescuento);
			panelLateral.add(descuento);
			panelLateral.add(borrar);
			panelLateral.add(cobrar);
			
			GridBagConstraints c = new GridBagConstraints();
			
			
			c.gridx=0;
			c.gridy=0;
			c.gridheight=13;
			c.fill=GridBagConstraints.BOTH;
			
			
			this.add(panelLateral,c);
			
			c.fill=GridBagConstraints.NONE;
			
			
			c.gridx = 1;
			c.gridy = 0;
			c.gridwidth=2;
			c.gridheight = 1;
			c.fill = GridBagConstraints.CENTER;
			
			this.add(prod, c);
			
			c.gridy = 5;
			this.add(ped,c);
			
			
			
			c.gridx = 1;
			c.gridy = 1;
			c.gridwidth = 2;
			c.gridheight = 3;
			c.fill= GridBagConstraints.BOTH;
	
			this.add(panelArriba, c);
			
			c.gridy=6;
			c.gridheight=2;
			
			
			this.add(panelAbajo, c);
			c.gridx = 6;
			c.gridy = 10;
			c.gridwidth = 1;
			c.gridheight = 5;
			c.anchor = GridBagConstraints.SOUTHEAST;
			c.fill = GridBagConstraints.VERTICAL;
			
			this.add(importe, c);
			
			
			
			c.gridx=0;
			c.gridy=0;
			c.fill = GridBagConstraints.BOTH;
			
//			this.add(quiensoy,c);
//			c.gridy=1;
//			this.add(nombre,c);
//			c.gridy=2;
//			this.add(login,c);
//			c.gridy=3;
//			c.gridheight=1;
//			c.weighty=0.0;
//			this.add(cobrar,c);
//			
//			
//			c.gridy=4;
//			c.gridheight=2;
//			c.fill= GridBagConstraints.BOTH;
//			this.add(borrar,c);

		
		
		
			
		}
		
	
	
	public static void main(String[] args) 
		{
			Interfaz TPV = Interfaz.GetSingleton();
			TPV.Refrescar();
			
		}
	
	public void CreaProductos()
		{
			
			productosDisponibles.add(new Producto("Cerveza", 1.5f));
			productosDisponibles.add(new Producto("Coca-cola", 1.0f));
			productosDisponibles.add(new Producto("Fanta", 1.0f));
			productosDisponibles.add(new Producto("Fuet", 1.5f));
			productosDisponibles.add(new Producto("Macarrones VIN", 20.0f));
			productosDisponibles.add(new Producto("Pedro Ximenez", 10.0f));
			productosDisponibles.add(new Producto("Sushi", 2.4f));
			productosDisponibles.add(new Producto("Campurrianas", 3.0f));
			productosDisponibles.add(new Producto("Cachalote", 5.5f));
			productosDisponibles.add(new Producto("Buey", 5.0f));
			productosDisponibles.add(new Producto("Estofado", 3.7f));
			productosDisponibles.add(new Producto("Menta poleo", 0.85f));
		}
	
	public void InicializarBotonesArriba()
		{
			for(int i = 0; i<productosDisponibles.size();i++)
				{
					botonAddPedido.add(new BotonProducto(productosDisponibles.get(i)));
				}
			
		}
	
	public void BotonesPanelArriba()
		{

			for(int i = 0; i<botonAddPedido.size();i++)
				{
				panelArriba.add(botonAddPedido.get(i));	
				botonAddPedido.get(i).setListenerArriba();
				}
		}
	
	public void PonerBotonAbajo(BotonProducto b)
		{
			b.setListenerAbajo();
			botonDeletePedido.add(b);
			panelAbajo.add(b);
			
		}
	
	public void Refrescar()
		{
		this.CalcularPrecio();
		SwingUtilities.updateComponentTreeUI(this); 
		this.validateTree();
		
		}
	
	public void CalcularPrecio()
		{
			float im = 0f;
			for(int i = 0; i<botonDeletePedido.size();i++)
				{
					im = im + botonDeletePedido.get(i).GetProducto().getPrecio();
				}
			importe.setText("Total a pagar: " +String.format("%.2f", im) + "�");
			importe.setFont(new Font("Times New Roman", Font.BOLD, 20));
			Border border = LineBorder.createBlackLineBorder();
			importe.setBorder(border);
		}
	public JPanel PanelAbajo()
	{
		return panelAbajo;
	}
	
	
	public void panelLateralListeners()
		{
		
		
		
		ActionListener b = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				botonDeletePedido.clear();
				panelAbajo.removeAll();
				CalcularPrecio();
				Refrescar();
				
			}
		};
		
		borrar.addActionListener(b);
		cobrar.addActionListener(b);
		

		ActionListener l = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				quiensoy.setText( nombre.getText());
				
			}
		};
		login.addActionListener(l);
		
		ActionListener d = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Producto p = new Producto("Descuento", -1.0f);
				BotonProducto b = new BotonProducto(p);
				PonerBotonAbajo(b);
				CalcularPrecio();
				Refrescar();
				
			}
		};
		descuento.addActionListener(d);
		
	
		
		
		

		
		}


}
