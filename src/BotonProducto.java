import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;


public class BotonProducto extends JButton {
	private Producto producto;

	
	public BotonProducto(Producto p)
		{
			producto = new Producto(p.getNombre(), p.getPrecio());
			String texto = new String("<html>"+producto.getNombre()+"<p style=text-align:center>"+Float.toString(producto.getPrecio())+"�</p>" + "</html>");
			
			this.setText(texto);
			this.setSize(20, 20);
			this.setVisible(true);
			
			
		}
	public void BorraBoton()
		{
			this.setVisible(false);
			producto.setPrecio(0f);
			producto.setNombre("nada");
			Interfaz.GetSingleton().PanelAbajo().remove(this);
		}
	
	public Producto GetProducto()
		{
			return producto;
		}
	
	
	
	public void setListenerArriba()
		{
		ActionListener l = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				BotonProducto nuevo = new BotonProducto(new Producto(GetProducto().getNombre(), GetProducto().getPrecio()));
				Interfaz.GetSingleton().PonerBotonAbajo(nuevo);
				Interfaz.GetSingleton().Refrescar();
				
			}
		};
		
		this.addActionListener(l);
		}
	
	public void setListenerAbajo()
		{
			ActionListener l = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				BorraBoton();
				Interfaz.GetSingleton().Refrescar();
				
			}
		};
		
		this.addActionListener(l);

		}
	

											}
	
	
		
	

