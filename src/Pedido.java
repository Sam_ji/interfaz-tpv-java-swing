
import java.util.ArrayList;


public class Pedido {

	private ArrayList<Producto> listaPedido;
	private float importe;
	
		
		public Pedido()
			{
				listaPedido = new ArrayList<Producto>();
				importe = 0;
			}
		
		public void InsertarProducto(Producto nuevoProducto)
			{
				listaPedido.add(nuevoProducto);
				importe = importe + nuevoProducto.getPrecio();
			}
		
		public void BorrarProducto(Producto todeleteProducto)
			{
				boolean encontrado = false;
				int contador = 0;
					
					while(!encontrado && contador<listaPedido.size())
						{
							if(listaPedido.get(contador).getNombre().equals(todeleteProducto.getNombre())) //Puede que explote
								{
									listaPedido.remove(contador);
									encontrado = true;
								}
							
						
						}
			}
		
		public void AplicarDescuento(float restar)
			{
			importe = importe - restar;	
			}
	
		public void CobrarPedido()
			{
			listaPedido.clear();
			importe = 0;
			}
}
